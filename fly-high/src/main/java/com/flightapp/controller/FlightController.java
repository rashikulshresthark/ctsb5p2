package com.flightapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flightapp.entities.Flight;
import com.flightapp.exceptions.FlightNotFoundException;
import com.flightapp.services.FlightService;

@RestController
@RequestMapping("/flights")
public class FlightController {
	
	@Autowired
	private FlightService flightService;
	
	@GetMapping("/")
	public List<Flight> getAllFlights(){
		return flightService.getAllFlights();
	}
	
	@GetMapping("/{flightNumber}")
	public Flight getFlightById(@PathVariable int flightNumber) throws FlightNotFoundException{
		System.out.println("Searching flight by ID");
		return flightService.findFlightById(flightNumber);
	}
	

}

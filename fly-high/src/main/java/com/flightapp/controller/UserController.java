package com.flightapp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user/")
public class UserController {
	
	@GetMapping("/greet")
	@ResponseBody
	public String greetUser() {
		return "Welcome to Fly-High";
	}
	
	@GetMapping("/greet/{firstName}")
	public String greetUser(@PathVariable String firstName) {
		String result = (firstName.charAt(0)+"").toUpperCase()+firstName.substring(1).toLowerCase();
		return "Hi "+ result + "\nWelcome to Fly-High";
	}
	
}

package com.flightapp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.flightapp.entities.Flight;

public interface FlightRepo extends JpaRepository<Flight, Integer>{

}

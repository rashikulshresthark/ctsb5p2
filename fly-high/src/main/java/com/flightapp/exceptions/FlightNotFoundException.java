package com.flightapp.exceptions;

@SuppressWarnings("serial")
public class FlightNotFoundException extends Exception {
	
	public FlightNotFoundException() { 
		super(); 
		}
	
	public FlightNotFoundException(String f) { 
		super(f); 
		}
	
	public FlightNotFoundException(Exception f) { 
		super(f); 
		}
	
	public FlightNotFoundException(String f, Exception e) { 
		super(f, e); 
		}

}

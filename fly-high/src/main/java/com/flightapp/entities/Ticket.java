package com.flightapp.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Ticket{ 
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int pnr;	
	
	public int getPnr() {
		return pnr;
	}

	public void setPnr(int pnr) {
		this.pnr = pnr;
	}

	public Ticket() {
	}

	public Ticket(int pnr) {
		super();
		this.pnr = pnr;
	}
	
}

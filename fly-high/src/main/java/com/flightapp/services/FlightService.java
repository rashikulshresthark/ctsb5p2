package com.flightapp.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flightapp.entities.Flight;
import com.flightapp.exceptions.FlightNotFoundException;
import com.flightapp.repo.FlightRepo;

@Service
public class FlightService {
	
	@Autowired
	private FlightRepo repo;
	
	public List<Flight> getAllFlights() {
		return repo.findAll();
	}

	public Flight findFlightById(int id) throws FlightNotFoundException {
		Optional<Flight> optional = repo.findById(id);
		if(optional.isPresent()) {
			return optional.get();
		} else {			
			throw new FlightNotFoundException("Flight with id "+id+" not found in database.");
		}
	}

	public Flight saveMovie(Flight flight) {
		return repo.save(flight);
	}

}
